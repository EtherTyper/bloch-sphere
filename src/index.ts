/// <reference path="../node_modules/@types/mathjax/index.d.ts"/>
import { Engine } from 'babylonjs';
import Visualisation from './visualisation'
import 'mousetrap';
import './index.css';
import { QubitState } from './communication';

const canvas = document.querySelector("#renderCanvas") as HTMLCanvasElement;

const engine = new Engine(canvas, true);

if (!document.cookie.split(';').filter((item) => {
    return item.includes('state=')
}).length) {
    document.cookie = `state=${JSON.stringify([
        { real: 1.0, imaginary: 0.0},
        { real: 0.0, imaginary: 0.0 }
    ])}`
}

let initialState = JSON.parse(
    document.cookie.replace(/(?:(?:^|.*;\s*)state\s*\=\s*([^;]*).*$)|^.*$/, "$1")
) as QubitState

const game = new Visualisation(engine, initialState);

// Register a render loop to repeatedly render the scene
engine.runRenderLoop(game.scene.render.bind(game.scene));

// Watch for browser/canvas resize events
window.addEventListener("resize", engine.resize.bind(engine));