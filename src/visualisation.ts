import { Scene, Engine, StandardMaterial, Vector3, Color4, HemisphericLight, Mesh, ArcRotateCamera } from 'babylonjs';
import { QubitState, Compute } from './communication'
import 'mousetrap';

const canvas = document.querySelector("#renderCanvas") as HTMLCanvasElement;
const stateFormula = document.querySelector('#stateFormula') as HTMLElement;
const buttonI = document.querySelector('#i') as HTMLButtonElement;
const buttonH = document.querySelector('#h') as HTMLButtonElement;
const buttonSqrtNOT = document.querySelector('#sqrtNOT') as HTMLButtonElement;
const buttonX = document.querySelector('#x') as HTMLButtonElement;
const buttonY = document.querySelector('#y') as HTMLButtonElement;
const buttonZ = document.querySelector('#z') as HTMLButtonElement;
const buttonR = document.querySelector('#r') as HTMLButtonElement;
const thetaInput = document.querySelector('#theta') as HTMLInputElement;
const buttonMeasure = document.querySelector('#measure') as HTMLButtonElement;

export default class Visualization {
    // Object references and constants.
    scene: Scene;
    private camera: ArcRotateCamera;
    private light: HemisphericLight;
    private sphere: Mesh;
    private vectorDrawing: Mesh;

    // Application State
    private state: QubitState;
    
    makeTextPlane(text, color, size) {
        let dynamicTexture = new BABYLON.DynamicTexture("DynamicTexture", 50, this.scene, true);
        dynamicTexture.hasAlpha = true;
        dynamicTexture.drawText(text, 5, 40, "bold 36px Arial", color, "transparent", true);

        let material = new StandardMaterial("TextPlaneMaterial", this.scene);
        material.backFaceCulling = false;
        material.diffuseTexture = dynamicTexture;

        let plane = Mesh.CreatePlane("TextPlane", size, this.scene, true);
        plane.material = material;
        
        return plane;
    };

    constructor(engine: Engine, state: QubitState) {
        this.scene = new Scene(engine);
        this.scene.clearColor = new Color4(0.5, 0.5, 0.5, 0.7);

        this.camera = new BABYLON.ArcRotateCamera("Camera", 0, 0, 5, new BABYLON.Vector3(0, 0, 0), this.scene);
        this.scene.activeCamera = this.camera;
        this.camera.attachControl(canvas, true);
        this.camera.setPosition(new Vector3(0, 0, -5));

        this.camera.lowerRadiusLimit = 5;
        this.camera.upperRadiusLimit = 5;

        let onMarker = this.makeTextPlane("|1⟩", "black", 0.2);
        let offMarker = this.makeTextPlane("|0⟩", "black", 0.2);

        onMarker.position = new Vector3(0, 1.15, 0);
        offMarker.position = new Vector3(0, -1.15, 0);

        this.light = new HemisphericLight("light", new Vector3(0, 1, 0), this.scene);
        this.light.intensity = .5;

        this.sphere = Mesh.CreateSphere("sphere", 20, 2, this.scene);
        this.sphere.visibility = 0.5;

        this.vectorDrawing = Mesh.CreateLines("vector", [
            new Vector3(0, 0, 0),
            new Vector3(0, 0, 0)
        ], this.scene, true);

        buttonI.onclick = async () => this.updateState(await Compute.apply(this.state, 'I'));
        buttonH.onclick = async () => this.updateState(await Compute.apply(this.state, 'H'));
        buttonSqrtNOT.onclick = async () => this.updateState(await Compute.apply(this.state, 'sqrtNOT'));
        buttonX.onclick = async () => this.updateState(await Compute.apply(this.state, 'X'));
        buttonY.onclick = async () => this.updateState(await Compute.apply(this.state, 'Y'));
        buttonZ.onclick = async () => this.updateState(await Compute.apply(this.state, 'Z'));
        buttonR.onclick = async () => this.updateState(await Compute.rotate(this.state, thetaInput.valueAsNumber));
        buttonMeasure.onclick = async () => this.updateState(await Compute.measure(this.state));

        this.updateState(state);
    }

    async updateState(state: QubitState) {
        this.state = state;
        document.cookie = `state=${JSON.stringify(this.state)}`

        stateFormula.innerText = `
            $$
                |\\psi\\rangle =

                \\begin{bmatrix}
                ${this.state[0].real.toFixed(3)} + ${this.state[0].imaginary.toFixed(3)}i \\\\
                ${this.state[1].real.toFixed(3)} + ${this.state[1].imaginary.toFixed(3)}i \\\\
                \\end{bmatrix}
            $$
        `

        this.vectorDrawing.setEnabled(false);

        this.vectorDrawing = Mesh.CreateLines("vector", [
            new Vector3(0, 0, 0),
            Compute.graphical_vector(await Compute.bloch_vector(this.state))
        ], this.scene, true);

        MathJax.Hub.Typeset(stateFormula, () => {});
    }
}