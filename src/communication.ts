import { Vector3 } from "babylonjs";

export type DoubleVector = [number, number, number];

export interface Complex {
    real: number;
    imaginary: number;
}

export type QubitState = [Complex, Complex];

export type Operation = 'I' | 'H' | 'sqrtNOT' | 'X' | 'Y' | 'Z';

const backendHost = 'http://127.0.0.1:7000';

export class Compute {
    static async bloch_vector(state: QubitState): Promise<DoubleVector> {
        let bloch_url = `${backendHost}/bloch_vector?state=${btoa(JSON.stringify(state))}`
        let bloch_vector = await (await fetch(bloch_url)).json()

        return bloch_vector
    }

    static async apply(state: QubitState, operation: Operation): Promise<QubitState> {
        let apply_url = `${backendHost}/apply?state=${btoa(JSON.stringify(state))}&gate=${operation}`
        let mutated_state = await (await fetch(apply_url)).json()

        return mutated_state
    }

    static async measure(state: QubitState): Promise<QubitState> {
        let measure_url = `${backendHost}/measure?state=${btoa(JSON.stringify(state))}`
        let measured_state = await (await fetch(measure_url)).json()

        return measured_state
    }

    static async rotate(state: QubitState, theta: number): Promise<QubitState> {
        let rotate_url = `${backendHost}/rotate?state=${btoa(JSON.stringify(state))}&theta=${theta}`

        let rotated_state = await (await fetch(rotate_url)).json()

        return rotated_state
    }

    static graphical_vector(vector: DoubleVector): Vector3 {
        // y-axis goes up in Babylon's coordinate system.
        // The other coordinates can be justified as to preserve the cross product.
        return new Vector3(
            vector[1],
            vector[2],
            vector[0]
        )
    }
}