let fetch = require('node-fetch');
let atob = require('atob');
let btoa = require('btoa');
(async () => {
    let state = [
        { real: 0.0, imaginary: 0.707 },
        { real: 0.707, imaginary: 0.0 }
    ]

    console.log('original_state')
    console.log(state)

    let bloch_url = `http://127.0.0.1:7000/bloch_vector?state=${btoa(JSON.stringify(state))}`
    let bloch_vector = await (await fetch(bloch_url)).json()

    let apply_url = `http://127.0.0.1:7000/apply?state=${btoa(JSON.stringify(state))}&gate=${'I'}`
    let mutated_state = await (await fetch(apply_url)).json()

    let measure_url = `http://127.0.0.1:7000/measure?state=${btoa(JSON.stringify(state))}`
    let measured_state = await (await fetch(measure_url)).json()

    let rotate_url = `http://127.0.0.1:7000/rotate?state=${btoa(JSON.stringify(state))}&theta=${Math.PI}`
    let rotated_state = await (await fetch(rotate_url)).json()

    console.log('bloch_vector')
    console.log(bloch_vector)

    console.log('mutated_state')
    console.log(mutated_state)

    console.log('measured_state')
    console.log(measured_state)

    console.log('rotated_state')
    console.log(rotated_state)
})()